import docker, tarfile, time, sys
from io import BytesIO

"""
	Function BuildProgram
	Compile a PLC from a ST Program
	Program 	=>	String containing PLC Program
	Program_id	=> 	Generated ID Of the program
	Return		=> 	Status, Build_Logs, Tar_Program
"""

def TarStreamFromContent(Program):
    #Create tar stream and file
    tarstream = BytesIO()
    tar = tarfile.TarFile(fileobj=tarstream, mode='w')

    #Create and encode tar file
    file_data = Program.encode('utf8')
    tarinfo = tarfile.TarInfo(name='program.st')
    tarinfo.size = len(file_data)
    tarinfo.mtime = time.time()

    #Add File to the stream
    tar.addfile(tarinfo, BytesIO(file_data))

    #Close tar creation
    tar.close()

    #This is usefull apparently
    tarstream.seek(0)

    return tarstream

def BuildProgram(Program):
    client=docker.from_env()

    #Create new container
    bc = client.containers.create("csaw/plc-builder")

    #Copy file into container
    pr = bc.put_archive(path='/build/input/',data=TarStreamFromContent(Program))

    #Start container
    bc.start()

    #Wait until the container stops
    exit_code = bc.wait(timeout=300)

    #Get the build logs
    build_logs = bc.logs()

    #Error in compilation
    if exit_code != 0:
       bc.remove()
       return -1,build_logs,bytearray()

    #GetOutput
    strm, stat = bc.get_archive('/build/output/openplc')

    #Read output stream
    tar_program = strm.read()

    #Remove
    bc.remove()

    return 0,build_logs,tar_program


if __name__ == '__main__':
    argv = sys.argv
    print(argv)

    if len(argv) != 3:
        print("Usage: python3 PLCBuilderControl.py program.st output.tar")
        sys.exit(-1)

    #Get file content
    content = ""
    with open(argv[1]) as f: content = f.read()

    #Execute Build function
    status, build_logs, tar_program = BuildProgram(content)

    #Show Status and Buils_logs
    print("Status: "+str(status))
    print("build_logs:"+str(build_logs))

    #Status = 0 => Went well, we can write the program in a file
    if status == 0:
        with open(argv[2],'wb') as f:
            f.write(tar_program)
        print("File "+argv[2]+" written")
