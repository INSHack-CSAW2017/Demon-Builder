from DBConnect import Connect

def GetPendingCompilations():
    connection = Connect()
    cursor = connection.cursor()

    cursor.execute(
        "SELECT program_id,source FROM Programs_BuilderDemon"
    )
    queue = []
    results = cursor.fetchall()
    for result in results:
        queue.append(result)

    cursor.close()
    connection.close()

    return queue

def SetProgram(IdProgram,Tar_Program,status,Build_Logs):
    connection = Connect()
    cursor = connection.cursor()


    #Status != 0 => Compilation failed
    if status != 0:
        cursor.execute(
            "UPDATE Programs_BuilderDemon SET status=3,build_log=%s WHERE program_id=%s",
            (Build_Logs,IdProgram)
        )

    else:
        cursor.execute(
            "UPDATE Programs_BuilderDemon SET tar_program=%s,status=2,build_log=%s WHERE program_id=%s",
            (Tar_Program,Build_Logs,IdProgram)
        )

    connection.commit()
    cursor.close()
    connection.close()
