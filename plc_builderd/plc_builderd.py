#!/usr/bin/python3

import time,sys,traceback,logging,daemonize
from PLCBuilderControl import BuildProgram
from DBManage import GetPendingCompilations,SetProgram

name = "plc_builderd"
pid = "/var/run/"+name+".pid"

def work():
    #Create logger
    logging.basicConfig(filename='/var/log/plc_builderd.log', level=logging.INFO)

    #Say it's started
    logging.info("Builder started working")
    while True:

        #Get incoming work
        compilations = []
        try:
            logging.debug('Get pendings builds')
            compilations = GetPendingCompilations()
        except Exception as ex:
            logging.error('Error connecting to database: '+str(ex))

        for id,source in compilations:

            #Set flag to know if we had a problem during build
            build = False

            #Build
            try:
                logging.info("Compiling plc "+str(id))
                status,build_logs,tar_program = BuildProgram(source)
                build = True
            except Exception as ex:
                logging.error('Error Building PLC: '+str(ex))

            #Set output of the program
            if build:
                try:
                    SetProgram(id,tar_program,status,build_logs)
                except Exception as ex:
                    logging.error('Error Building PLC: '+str(ex))



        #Sleep
        logging.debug("Going to sleep")
        time.sleep(5)

daemon = daemonize.Daemonize(app=name, pid=pid, action=work)
daemon.start()
