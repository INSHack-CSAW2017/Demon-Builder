# Démon Builder

## Install
Vous devez d'abord construire l'image csaw/plc-builder via le repo:

https://gitlab.com/INSHack-CSAW2017/Docker_PLCBuilder    

Et avoir la base de donnée du site de controle de PLC sur un serveur mariadb

https://gitlab.com/INSHack-CSAW2017/Website-PLCControl

    sudo apt-get install python3 python3-pip
    sudo python3 -m pip install --upgrade pip
    sudo pip3 install docker mysqlclient daemonize
    sudo ./install.sh
    sudo service plc_builderd start

## TODO
- Watchdog
- Permissions

## Description
Ce démon prend regarde dans la base de données, regarde 
si un programe attends d'être compilé, le compile et écrit les logs, le status,
et le résultat si le programme a réussi a être compilé dans la base de données.

## Test d'un build particulier

Ce script, lorsqu'il est appelé directement, permet de construire un plc.

Vous devez d'abord construire l'image csaw/plc-builder via le repo:
https://gitlab.com/INSHack-CSAW2017/Docker_PLCBuilder

    apt-get install python3 python3-pip 
    pip install docker

Vous pouvez ensuite tester le script avec un exemple valide et un exemple 
invalide dans le repertoire tests

    sudo python3 PLCBuilderControl.py /tests/invalidprogram.st output.tar
    sudo python3 PLCBuilderControl.py /tests/program.st output.tar

