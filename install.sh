#!/bin/bash

#Install required
apt-get install -y python3 python3-pip python3-dev mariadb-server libmariadbclient-dev
pip3 install docker mysqlclient daemonize

#Copy executables
cp -R plc_builderd /usr/sbin/

#Move init file
cp init-script/plc_builderd /etc/init.d/plc_builderd
sudo chmod 0755 /etc/init.d/plc_builderd

#Reload daemons
systemctl daemon-reload

#Add at start
sudo update-rc.d plc_builderd defaults

#Automatic start at install
sudo service plc_builderd start
